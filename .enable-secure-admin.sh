#!/bin/bash
DIR='/opt/glassfish4'
export JAVA_HOME=/opt/java
export AS_JAVA=/opt/java
export GLASSFISH_HOME=/opt/glassfish4
export PATH=$PATH:$JAVA_HOME/bin:$GLASSFISH_HOME/bin:$AS_JAVA
$DIR/bin/asadmin start-domain domain1

# enable-secure-admin
$DIR/bin/asadmin --host localhost --port 4848 enable-secure-admin

# Restart
$DIR/stop.sh
sleep 3
$DIR/start.sh
