- Install Java

```
cd /opt/
git clone git@bitbucket.org:suryacom/java.git
sudo chown $USER:$GROUP -R /opt/java
```

Make sure there is no installed native java pakages on system:  
Unless U use minimal install ... !!!  
In OpenSuse:

```
rpm -qa | grep java
rpm -e
```

In Ubuntu:  
No native java (open-jdk) installed , so just put !!! JAVA_HOME in user environment.  
  
- Set JAVA_HOME on user environment : 

```
vi ~/.profile ; ~/.bashrc

export JAVA_HOME=/opt/java
export GLASSFISH_HOME=/opt/glassfish4
export PATH=$JAVA_HOME/bin:$GLASSFISH_HOME/bin:$PATH
```
- Install GlassFish

```
cd /opt/
git clone git@bitbucket.org:suryacom/glassfish4.git
chmod 755 -R /opt/glassfish4
```
- Setup proper ownership 

```
adduser --disabled-password --ingroup www-data --home /opt/glassfish4 glassfish
chown -hR glassfish:www-data /opt/glassfish4

or;atau

sudo chown -R $USER:$GOURP /opt/glassfish4
```
- Create init script  
To run it as a service, you can create the following init script in /etc/init.d/glassfish4:  

```
Change GLASSFISH_USER variable to user that runs glassfish 

GLASSFISH_USER=glassfish

Example :
GLASSFISH_USER=ubuntu
```

```
#!/bin/bash
### BEGIN INIT INFO
# Provides:          glassfish
# Required-Start:    $remote_fs
# Required-Stop:     umountnfs
# X-Stop-After:      sendsigs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: GlassFish 
# Description:       GlassFish 4 Application Server
### END INIT INFO

# VARIABLE
JAVA_HOME=/opt/java
 export JAVA_HOME

AS_JAVA=/opt/java
 export AS_JAVA

GLASSFISH_HOME=/opt/glassfish4
 export GLASSFISH_HOME

GLASSFISH_USER=glassfish
 export GLASSFISH_USER

PATH=$PATH:$JAVA_HOME/bin:$GLASSFISH_HOME:$AS_JAVA
 export PATH

start() {
	echo -n "Starting Glassfish: "
	su $GLASSFISH_USER -c "$GLASSFISH_HOME/bin/asadmin start-domain domain1"
	sleep 2
	echo "done"
}

stop() {
	echo -n "Stopping Glassfish: "
	su $GLASSFISH_USER -c "$GLASSFISH_HOME/bin/asadmin stop-domain domain1"
	echo "done"
}

case "$1" in
	start)
		start
	;;
	stop)
		stop
	;;
	restart)
		stop
		start
	;;
	*)
		echo $"Usage: glassfish {start|stop|restart}"
		exit
esac
```


once created, set the executable bit on the script using chmod  

```
chmod +x /etc/init.d/glassfish4
```

and use the service command to start, stop or restart the glassfish4 service, for example:  

```
service glassfish4 start
```

Now, to add it to your virtual server startup, you need to execute:  

```
update-rc.d -f glassfish4 defaults
```

- GlassFish Tunning

```
- default-config
  - JVM Settings - JVM Options :
    -XX:MaxPermSize=2048m
    -server
    -Xmx4096m
    -Xms4096m

- server-config
  - JVM Settings - JVM Options :
    -XX:MaxPermSize=4096m
    -server
    -Xmx8192m
    -Xms8192m
```

- Create JDBC  
an example:

```
- JDBC Resources : 
  - jdbc/newnagamas

- JDBC Connection Pools
  - name			        : newnagamas
  - Resource Type		    : java.sql.Driver
  - Database Driver Vendor	: MySQL (com.mysql.jdbc.Driver)
  - user                    : your_user
  - password                : your_password
  - url 			        : jdbc:mysql://127.0.0.1:3306/newnagamas
  - Maximum Pool Size		: 2000
```
- How to roleback code:  

```
git log --oneline

b3d92c5 <patch-002>
955929c <patch-001>

git reset --hard 955929c
git push -f origin master
```
