#!/bin/bash
DIR='/opt/glassfish4'
export GLASSFISH_HOME=/opt/glassfish4
export JAVA_HOME=/opt/java
export AS_JAVA=/opt/java
export PATH=$PATH:$JAVA_HOME/bin:$GLASSFISH_HOME/bin:$AS_JAVA
$DIR/bin/asadmin stop-domain domain1
